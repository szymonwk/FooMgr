export interface IStatistics {
    counter?: string;
    max?: string;
    mean?: string;
    min?: string;
}

export class Statistics implements IStatistics {
    constructor(public counter?: any, public max?: string, public mean?: string, public min?: string) {
        this.counter = counter ? counter : null;
        this.max = max ? max : null;
        this.mean = mean ? mean : null;
        this.min = min ? min : null;
    }
}
