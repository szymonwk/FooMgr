import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IReports } from 'app/shared/model/reports.model';

type EntityResponseType = HttpResponse<IReports>;
type EntityArrayResponseType = HttpResponse<IReports[]>;

@Injectable({ providedIn: 'root' })
export class ReportsService {
    public resourceUrl = SERVER_API_URL + 'api/reports';
    public resourceSearchUrl = SERVER_API_URL + 'api/_search/reports';

    constructor(protected http: HttpClient) {}

    create(reports: IReports): Observable<EntityResponseType> {
        return this.http.post<IReports>(this.resourceUrl, reports, { observe: 'response' });
    }

    update(reports: IReports): Observable<EntityResponseType> {
        return this.http.put<IReports>(this.resourceUrl, reports, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IReports>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IReports[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IReports[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
