import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IReports } from 'app/shared/model/reports.model';
import { ReportsService } from './reports.service';
import { ITraining } from 'app/shared/model/training.model';
import { TrainingService } from 'app/entities/training';

@Component({
    selector: 'jhi-reports-update',
    templateUrl: './reports-update.component.html'
})
export class ReportsUpdateComponent implements OnInit {
    reports: IReports;
    isSaving: boolean;

    trainings: ITraining[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected reportsService: ReportsService,
        protected trainingService: TrainingService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ reports }) => {
            this.reports = reports;
        });
        this.trainingService.query({ filter: 'reports-is-null' }).subscribe(
            (res: HttpResponse<ITraining[]>) => {
                if (!this.reports.training || !this.reports.training.id) {
                    this.trainings = res.body;
                } else {
                    this.trainingService.find(this.reports.training.id).subscribe(
                        (subRes: HttpResponse<ITraining>) => {
                            this.trainings = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.reports.id !== undefined) {
            this.subscribeToSaveResponse(this.reportsService.update(this.reports));
        } else {
            this.subscribeToSaveResponse(this.reportsService.create(this.reports));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IReports>>) {
        result.subscribe((res: HttpResponse<IReports>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackTrainingById(index: number, item: ITraining) {
        return item.id;
    }
}
