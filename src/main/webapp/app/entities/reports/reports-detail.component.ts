import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IReports } from 'app/shared/model/reports.model';

@Component({
    selector: 'jhi-reports-detail',
    templateUrl: './reports-detail.component.html'
})
export class ReportsDetailComponent implements OnInit {
    reports: IReports;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ reports }) => {
            this.reports = reports;
        });
    }

    previousState() {
        window.history.back();
    }
}
