import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IGroups } from 'app/shared/model/groups.model';
import { GroupsService } from './groups.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
    selector: 'jhi-groups-update',
    templateUrl: './groups-update.component.html'
})
export class GroupsUpdateComponent implements OnInit {
    groups: IGroups;
    isSaving: boolean;

    jhi_users: IUser[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected groupsService: GroupsService,
        protected jhi_userService: UserService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ groups }) => {
            this.groups = groups;
        });
        this.jhi_userService.query().subscribe(
            (res: HttpResponse<IUser[]>) => {
                this.jhi_users = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.groups.id !== undefined) {
            this.subscribeToSaveResponse(this.groupsService.update(this.groups));
        } else {
            this.subscribeToSaveResponse(this.groupsService.create(this.groups));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IGroups>>) {
        result.subscribe((res: HttpResponse<IGroups>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackJhi_userById(index: number, item: IUser) {
        return item.id;
    }
}
