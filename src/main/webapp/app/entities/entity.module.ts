import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { FooschoolGroupsModule } from './groups/groups.module';
import { FooschoolTrainingModule } from './training/training.module';
import { FooschoolReportsModule } from './reports/reports.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        FooschoolGroupsModule,
        FooschoolTrainingModule,
        FooschoolReportsModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FooschoolEntityModule {}
