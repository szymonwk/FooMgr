import { IUser } from 'app/core/user/user.model';

export interface IGroups {
    id?: number;
    groupName?: string;
    coach?: IUser;
}

export class Groups implements IGroups {
    constructor(public id?: number, public groupName?: string, public coach?: IUser) {}
}
