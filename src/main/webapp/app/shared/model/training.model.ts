import { Moment } from 'moment';
import { IGroups } from 'app/shared/model//groups.model';

export interface ITraining {
    id?: number;
    nextTrainingDate?: Moment;
    address?: string;
    isNotificationSent?: boolean;
    group?: IGroups;
}

export class Training implements ITraining {
    constructor(
        public id?: number,
        public nextTrainingDate?: Moment,
        public address?: string,
        public isNotificationSent?: boolean,
        public group?: IGroups
    ) {
        this.isNotificationSent = this.isNotificationSent || false;
    }
}
