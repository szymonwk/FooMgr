import { ITraining } from 'app/shared/model//training.model';

export interface IReports {
    id?: number;
    numberOfParticipats?: number;
    comment?: string;
    training?: ITraining;
}

export class Reports implements IReports {
    constructor(public id?: number, public numberOfParticipats?: number, public comment?: string, public training?: ITraining) {}
}
