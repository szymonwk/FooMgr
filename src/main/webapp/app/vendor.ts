/* after changing this file run 'npm run webpack:build' */
/* tslint:disable */
import '../content/scss/vendor.scss';

// Imports all fontawesome core and solid icons

import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faUser,
    faSort,
    faSortUp,
    faSortDown,
    faSync,
    faEye,
    faBan,
    faTimes,
    faArrowLeft,
    faSave,
    faPlus,
    faPencilAlt,
    faBars,
    faBalanceScale,
    faChartLine,
    faThList,
    faUserPlus,
    faRoad,
    faTachometerAlt,
    faHeart,
    faList,
    faBell,
    faBook,
    faHdd,
    faFlag,
    faWrench,
    faClock,
    faCloud,
    faSignOutAlt,
    faSignInAlt,
    faCalendarAlt,
    faChartBar,
    faSearch,
    faTrashAlt,
    faAsterisk,
    faArrowsAltH,
    faArrowDown,
    faArrowUp,
    faTasks,
    faAngry,
    faSortNumericDown,
    faTemperatureLow,
    faHome
} from '@fortawesome/free-solid-svg-icons';

// Adds the SVG icon to the library so you can use it in your page
library.add(faUser);
library.add(faArrowsAltH);
library.add(faSortNumericDown);
library.add(faArrowDown);
library.add(faArrowUp);
library.add(faAngry);
library.add(faTemperatureLow);
library.add(faSort);
library.add(faSortUp);
library.add(faSortDown);
library.add(faChartLine);
library.add(faChartBar);
library.add(faSync);
library.add(faEye);
library.add(faBan);
library.add(faBalanceScale);
library.add(faTimes);
library.add(faArrowLeft);
library.add(faSave);
library.add(faPlus);
library.add(faPencilAlt);
library.add(faBars);
library.add(faHome);
library.add(faThList);
library.add(faUserPlus);
library.add(faRoad);
library.add(faTachometerAlt);
library.add(faHeart);
library.add(faList);
library.add(faBell);
library.add(faTasks);
library.add(faBook);
library.add(faHdd);
library.add(faFlag);
library.add(faWrench);
library.add(faClock);
library.add(faCloud);
library.add(faSignOutAlt);
library.add(faSignInAlt);
library.add(faCalendarAlt);
library.add(faSearch);
library.add(faTrashAlt);
library.add(faAsterisk);

// jhipster-needle-add-element-to-vendor - JHipster will add new menu items here
