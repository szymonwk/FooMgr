package com.fschl.repository;

import com.fschl.domain.Groups;
import com.fschl.domain.Training;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;


/**
 * Spring Data  repository for the Training entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrainingRepository extends JpaRepository<Training, Long> {
    List<Training> findByGroupIn(List<Groups> groups);

    List<Training> findAllByNextTrainingDateEqualsAndIsNotificationSent(LocalDate date, boolean isNotificationSent);

    List<Training> findAllByNextTrainingDateEqualsAndIsNotificationSentFalse(LocalDate date);
}
