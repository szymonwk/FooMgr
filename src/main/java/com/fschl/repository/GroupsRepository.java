package com.fschl.repository;

import com.fschl.domain.Groups;
import com.fschl.domain.User;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Groups entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GroupsRepository extends JpaRepository<Groups, Long> {

//    List<Groups> findAllByUser(User user);
    List<Groups> findGroupsByCoach_Login(String login);
}
