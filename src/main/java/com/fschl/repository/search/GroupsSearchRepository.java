package com.fschl.repository.search;

import com.fschl.domain.Groups;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Groups entity.
 */
public interface GroupsSearchRepository extends ElasticsearchRepository<Groups, Long> {
}
