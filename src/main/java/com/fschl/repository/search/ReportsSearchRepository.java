package com.fschl.repository.search;

import com.fschl.domain.Reports;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Reports entity.
 */
public interface ReportsSearchRepository extends ElasticsearchRepository<Reports, Long> {
}
