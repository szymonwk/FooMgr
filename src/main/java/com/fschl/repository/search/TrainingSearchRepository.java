package com.fschl.repository.search;

import com.fschl.domain.Training;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Training entity.
 */
public interface TrainingSearchRepository extends ElasticsearchRepository<Training, Long> {
}
