package com.fschl.repository;

import com.fschl.domain.Reports;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;


/**
 * Spring Data  repository for the Reports entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReportsRepository extends JpaRepository<Reports, Long> {

    List<Reports> findAllByTrainingGroupCoachLogin(String aa);

    List<Reports> findAllByTrainingGroupCoachLoginAndNumberOfParticipatsNotNull(String login);

    List<Reports> findAll();

    List<Reports> findAllByNumberOfParticipatsNotNull();

    List<Reports> findAllByTrainingNextTrainingDateAfterAndTrainingGroupCoachLoginOrderByTrainingNextTrainingDateAsc(LocalDate since, String login);
}
