package com.fschl.batch;

import com.fschl.domain.Training;
import com.fschl.repository.TrainingRepository;
import com.fschl.service.MailService;
import com.fschl.web.rest.UserResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Component
public class TrainingReminderBatch {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    @Autowired
    TrainingRepository trainingRepository;

    @Autowired
    MailService mailService;

    @Scheduled(cron = "0 0 */2 * * *")
    @Transactional
    public void sendNotificationBatch(){
        log.info("Batch send mail notification");
        trainingRepository.findAllByNextTrainingDateEqualsAndIsNotificationSentFalse(LocalDate.now().plusDays(1)).stream().forEach(training -> sendTrainingNotification(training));
    }

    private void sendTrainingNotification(Training training){
        mailService.sendTrainingReminder(training.getGroup().getCoach());
        markAsSent(training);
    }

    private void markAsSent(Training training){
        Training notifiedTraining = training;
        notifiedTraining.setIsNotificationSent(true);
        trainingRepository.save(notifiedTraining);
    }
}
