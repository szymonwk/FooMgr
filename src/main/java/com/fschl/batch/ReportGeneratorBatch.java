package com.fschl.batch;

import com.fschl.service.ReportGenerationService;
import com.fschl.service.enums.ReportType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;

@Component
public class ReportGeneratorBatch {

    @Autowired
    ReportGenerationService reportGenerationService;

    @Scheduled(cron = "1 0 0 1 * *")
    public void generateExpense() throws IOException {
        reportGenerationService.generateReport(ReportType.EXPENSE, LocalDate.now());
    }
}
