package com.fschl.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Reports.
 */
@Entity
@Table(name = "reports")
@Document(indexName = "reports")
public class Reports implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "number_of_participats")
    private Long numberOfParticipats;

    @Column(name = "jhi_comment")
    private String comment;

    @OneToOne(optional = false)    @NotNull
    @JoinColumn(unique = true)
    private Training training;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumberOfParticipats() {
        return numberOfParticipats;
    }

    public Reports numberOfParticipats(Long numberOfParticipats) {
        this.numberOfParticipats = numberOfParticipats;
        return this;
    }

    public void setNumberOfParticipats(Long numberOfParticipats) {
        this.numberOfParticipats = numberOfParticipats;
    }

    public String getComment() {
        return comment;
    }

    public Reports comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Training getTraining() {
        return training;
    }

    public Reports training(Training training) {
        this.training = training;
        return this;
    }

    public void setTraining(Training training) {
        this.training = training;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Reports reports = (Reports) o;
        if (reports.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reports.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Reports{" +
            "id=" + getId() +
            ", numberOfParticipats=" + getNumberOfParticipats() +
            ", comment='" + getComment() + "'" +
            "}";
    }
}
