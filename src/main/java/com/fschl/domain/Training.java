package com.fschl.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Training.
 */
@Entity
@Table(name = "training")
@Document(indexName = "training")
public class Training implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "next_training_date", nullable = false)
    private LocalDate nextTrainingDate;

    @NotNull
    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "is_notification_sent")
    private Boolean isNotificationSent;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("")
    private Groups group;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getNextTrainingDate() {
        return nextTrainingDate;
    }

    public Training nextTrainingDate(LocalDate nextTrainingDate) {
        this.nextTrainingDate = nextTrainingDate;
        return this;
    }

    public void setNextTrainingDate(LocalDate nextTrainingDate) {
        this.nextTrainingDate = nextTrainingDate;
    }

    public String getAddress() {
        return address;
    }

    public Training address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean isIsNotificationSent() {
        return isNotificationSent;
    }

    public Training isNotificationSent(Boolean isNotificationSent) {
        this.isNotificationSent = isNotificationSent;
        return this;
    }

    public void setIsNotificationSent(Boolean isNotificationSent) {
        this.isNotificationSent = isNotificationSent;
    }

    public Groups getGroup() {
        return group;
    }

    public Training group(Groups groups) {
        this.group = groups;
        return this;
    }

    public void setGroup(Groups groups) {
        this.group = groups;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Training training = (Training) o;
        if (training.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), training.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Training{" +
            "id=" + getId() +
            ", nextTrainingDate='" + getNextTrainingDate() + "'" +
            ", address='" + getAddress() + "'" +
            ", isNotificationSent='" + isIsNotificationSent() + "'" +
            "}";
    }
}
