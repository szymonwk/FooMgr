package com.fschl.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fschl.domain.Reports;
import com.fschl.repository.ReportsRepository;
import com.fschl.repository.search.ReportsSearchRepository;
import com.fschl.security.AuthoritiesConstants;
import com.fschl.security.SecurityUtils;
import com.fschl.web.rest.errors.BadRequestAlertException;
import com.fschl.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Reports.
 */
@RestController
@RequestMapping("/api")
public class ReportsResource {

    private final Logger log = LoggerFactory.getLogger(ReportsResource.class);

    private static final String ENTITY_NAME = "reports";

    private final ReportsRepository reportsRepository;

    private final ReportsSearchRepository reportsSearchRepository;

    public ReportsResource(ReportsRepository reportsRepository, ReportsSearchRepository reportsSearchRepository) {
        this.reportsRepository = reportsRepository;
        this.reportsSearchRepository = reportsSearchRepository;
    }

    /**
     * POST  /reports : Create a new reports.
     *
     * @param reports the reports to create
     * @return the ResponseEntity with status 201 (Created) and with body the new reports, or with status 400 (Bad Request) if the reports has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/reports")
    @Timed
    public ResponseEntity<Reports> createReports(@Valid @RequestBody Reports reports) throws URISyntaxException {
        log.debug("REST request to save Reports : {}", reports);
        if (reports.getId() != null) {
            throw new BadRequestAlertException("A new reports cannot already have an ID", ENTITY_NAME, "idexists");
        }

        if (isAdminOrCurrentUser(reports)){
            Reports result = reportsRepository.save(reports);
            reportsSearchRepository.save(result);
            return ResponseEntity.created(new URI("/api/reports/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
        }
        log.debug("You have not access to post reports");
        return ResponseEntity.notFound().build();

    }

    private boolean isAdminOrCurrentUser(@RequestBody @Valid Reports reports) {
        return SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN) || (SecurityUtils.getCurrentUserLogin().isPresent() && SecurityUtils.getCurrentUserLogin().get().equals(reports.getTraining().getGroup().getCoach().getLogin()));
    }

    /**
     * PUT  /reports : Updates an existing reports.
     *
     * @param reports the reports to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated reports,
     * or with status 400 (Bad Request) if the reports is not valid,
     * or with status 500 (Internal Server Error) if the reports couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/reports")
    @Timed
    public ResponseEntity<Reports> updateReports(@Valid @RequestBody Reports reports) throws URISyntaxException {
        log.debug("REST request to update Reports : {}", reports);
        if (reports.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (isAdminOrCurrentUser(reports)) {
            Reports result = reportsRepository.save(reports);
            reportsSearchRepository.save(result);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, reports.getId().toString()))
                .body(result);
        }
        log.debug("You have not access to update reports");
        return ResponseEntity.notFound().build();
    }

    /**
     * GET  /reports : get all the reports.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of reports in body
     */
    @GetMapping("/reports")
    @Timed
    public List<Reports> getAllReports() {
        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)){
            Optional<String> currentUserLogin = SecurityUtils.getCurrentUserLogin();
            if (currentUserLogin.isPresent()){
                return reportsRepository.findAllByTrainingGroupCoachLogin(currentUserLogin.get());
            }
        }
        log.debug("REST request to get all Reports");
        return reportsRepository.findAll();
    }

    /**
     * GET  /reports/:id : get the "id" reports.
     *
     * @param id the id of the reports to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the reports, or with status 404 (Not Found)
     */
    @GetMapping("/reports/{id}")
    @Timed
    public ResponseEntity<Reports> getReports(@PathVariable Long id) {
        log.debug("REST request to get Reports : {}", id);
        Optional<Reports> reports = reportsRepository.findById(id);
        if (reports.isPresent() && isAdminOrCurrentUser(reports.get())) {
            return ResponseUtil.wrapOrNotFound(reports);
        }
        log.debug("You have not access to get report {}", id);
        return ResponseEntity.notFound().build();
    }

    /**
     * DELETE  /reports/:id : delete the "id" reports.
     *
     * @param id the id of the reports to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/reports/{id}")
    @Timed
    public ResponseEntity<Void> deleteReports(@PathVariable Long id) {
        log.debug("REST request to delete Reports : {}", id);

        Optional<Reports> reports = reportsRepository.findById(id);
        if (reports.isPresent() && isAdminOrCurrentUser(reports.get())) {
            reportsRepository.deleteById(id);
            reportsSearchRepository.deleteById(id);
            return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
        }
        log.debug("You have not access to delete report {}", id);
        return ResponseEntity.notFound().build();
    }

    /**
     * SEARCH  /_search/reports?query=:query : search for the reports corresponding
     * to the query.
     *
     * @param query the query of the reports search
     * @return the result of the search
     */
    @GetMapping("/_search/reports")
    @Timed
    public List<Reports> searchReports(@RequestParam String query) {
        log.debug("REST request to search Reports for query {}", query);
        List<Reports> reportsList = StreamSupport
            .stream(reportsSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            return reportsList;
        }
        Optional<String> currentUserLogin = SecurityUtils.getCurrentUserLogin();
        return reportsList.stream().filter(a -> !a.getTraining().getGroup().getCoach().getLogin().equals(currentUserLogin.get())).collect(Collectors.toList());
    }

}
