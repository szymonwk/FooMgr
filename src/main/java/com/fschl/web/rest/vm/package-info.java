/**
 * View Models used by Spring MVC REST controllers.
 */
package com.fschl.web.rest.vm;
