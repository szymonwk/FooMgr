package com.fschl.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fschl.domain.Groups;
import com.fschl.domain.User;
import com.fschl.repository.GroupsRepository;
import com.fschl.repository.search.GroupsSearchRepository;
import com.fschl.security.AuthoritiesConstants;
import com.fschl.security.SecurityUtils;
import com.fschl.web.rest.errors.BadRequestAlertException;
import com.fschl.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.mapstruct.ap.shaded.freemarker.template.utility.SecurityUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Groups.
 */
@RestController
@RequestMapping("/api")
public class GroupsResource {

    private final Logger log = LoggerFactory.getLogger(GroupsResource.class);

    private static final String ENTITY_NAME = "groups";

    private final GroupsRepository groupsRepository;

    private final GroupsSearchRepository groupsSearchRepository;

    public GroupsResource(GroupsRepository groupsRepository, GroupsSearchRepository groupsSearchRepository) {
        this.groupsRepository = groupsRepository;
        this.groupsSearchRepository = groupsSearchRepository;
    }

    /**
     * POST  /groups : Create a new groups.
     *
     * @param groups the groups to create
     * @return the ResponseEntity with status 201 (Created) and with body the new groups, or with status 400 (Bad Request) if the groups has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/groups")
    @Timed
    public ResponseEntity<Groups> createGroups(@Valid @RequestBody Groups groups) throws URISyntaxException {
        log.debug("REST request to save Groups : {}", groups);
        if (groups.getId() != null) {
            throw new BadRequestAlertException("A new groups cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Optional<String> currentUserLogin = SecurityUtils.getCurrentUserLogin();
        if (currentUserLogin.isPresent()) {
            if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN) && !groups.getCoach().getLogin().equals(currentUserLogin.get())) {
                throw new BadRequestAlertException("You can not add groups for different user than you", ENTITY_NAME, "idnull");
            }
        }


        Groups result = groupsRepository.save(groups);
        groupsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /groups : Updates an existing groups.
     *
     * @param groups the groups to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated groups,
     * or with status 400 (Bad Request) if the groups is not valid,
     * or with status 500 (Internal Server Error) if the groups couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/groups")
    @Timed
    public ResponseEntity<Groups> updateGroups(@Valid @RequestBody Groups groups) throws URISyntaxException {
        log.debug("REST request to update Groups : {}", groups);
        if (groups.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        Optional<String> currentUserLogin = SecurityUtils.getCurrentUserLogin();
        if ((groups.getCoach().getLogin().equals(currentUserLogin)) || SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)){
            Groups result = groupsRepository.save(groups);
            groupsSearchRepository.save(result);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, groups.getId().toString()))
                .body(result);
        }
        log.debug("You have not access to update group : {}", groups.getId());
        return ResponseEntity.notFound().build();
    }


    /**
     * GET  /groups : get all the groups.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of groups in body
     */
    @GetMapping("/groups")
    @Timed
    public List<Groups> getAllGroups() {
        log.debug("REST request to get Groups");
        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)){
            Optional<String> currentUserLogin = SecurityUtils.getCurrentUserLogin();
            if (currentUserLogin.isPresent()){
                return groupsRepository.findGroupsByCoach_Login(currentUserLogin.get());
            }
        }
        return groupsRepository.findAll();
    }

    /**
     * GET  /groups/:id : get the "id" groups.
     *
     * @param id the id of the groups to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the groups, or with status 404 (Not Found)
     */
    @GetMapping("/groups/{id}")
    @Timed
    public ResponseEntity<Groups> getGroups(@PathVariable Long id) {
        log.debug("REST request to get Groups : {}", id);
        Optional<Groups> groups = groupsRepository.findById(id);
        Optional<String> currentUserLogin = SecurityUtils.getCurrentUserLogin();
        if ((groups.isPresent() && groups.get().getCoach().getLogin().equals(currentUserLogin)) || SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)){
            return ResponseUtil.wrapOrNotFound(groups);
        }
        log.debug("You have not access to group : {}", id);
        return ResponseEntity.notFound().build();
    }

    /**
     * DELETE  /groups/:id : delete the "id" groups.
     *
     * @param id the id of the groups to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/groups/{id}")
    @Timed
    public ResponseEntity<Void> deleteGroups(@PathVariable Long id) {
        log.debug("REST request to delete Groups : {}", id);
        Optional<String> currentUserLogin = SecurityUtils.getCurrentUserLogin();
        Optional<Groups> groups = groupsRepository.findById(id);
        if ((groups.isPresent() && groups.get().getCoach().getLogin().equals(currentUserLogin.get())) || SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)){
            groupsRepository.deleteById(id);
            groupsSearchRepository.deleteById(id);
            return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
        }
        log.debug("You have not access to delete : {}", id);
        return ResponseEntity.notFound().build();
    }

    /**
     * SEARCH  /_search/groups?query=:query : search for the groups corresponding
     * to the query.
     *
     * @param query the query of the groups search
     * @return the result of the search
     */
    @GetMapping("/_search/groups")
    @Timed
    public List<Groups> searchGroups(@RequestParam String query) {
        log.debug("REST request to search Groups for query {}", query);
        List<Groups> groupsList = StreamSupport
            .stream(groupsSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
        Optional<String> currentUserLogin = SecurityUtils.getCurrentUserLogin();
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            return groupsList;
        }
        return groupsList.stream().filter(a -> !a.getCoach().getLogin().equals(currentUserLogin.get())).collect(Collectors.toList());
    }

}
