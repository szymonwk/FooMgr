package com.fschl.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fschl.domain.Groups;
import com.fschl.domain.Reports;
import com.fschl.domain.Training;
import com.fschl.repository.GroupsRepository;
import com.fschl.repository.ReportsRepository;
import com.fschl.repository.TrainingRepository;
import com.fschl.repository.search.TrainingSearchRepository;
import com.fschl.security.AuthoritiesConstants;
import com.fschl.security.SecurityUtils;
import com.fschl.web.rest.errors.BadRequestAlertException;
import com.fschl.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Training.
 */
@RestController
@RequestMapping("/api")
public class TrainingResource {

    private final Logger log = LoggerFactory.getLogger(TrainingResource.class);

    private static final String ENTITY_NAME = "training";

    private final TrainingRepository trainingRepository;

    private final TrainingSearchRepository trainingSearchRepository;

    private final GroupsRepository groupsRepository;

    private final ReportsRepository reportsRepository;

    public TrainingResource(TrainingRepository trainingRepository, TrainingSearchRepository trainingSearchRepository, GroupsRepository groupsRepository, ReportsRepository reportsRepository) {
        this.trainingRepository = trainingRepository;
        this.trainingSearchRepository = trainingSearchRepository;
        this.groupsRepository = groupsRepository;
        this.reportsRepository = reportsRepository;
    }

    /**
     * POST  /trainings : Create a new training.
     *
     * @param training the training to create
     * @return the ResponseEntity with status 201 (Created) and with body the new training, or with status 400 (Bad Request) if the training has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trainings")
    @Timed
    public ResponseEntity<Training> createTraining(@Valid @RequestBody Training training) throws URISyntaxException {
        log.debug("REST request to save Training : {}", training);
        if (training.getId() != null) {
            throw new BadRequestAlertException("A new training cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (isAdminOrCurrentUser(training)){
            Training result = trainingRepository.save(training);
            trainingSearchRepository.save(result);
            Reports reports = new Reports();
            reports.setTraining(result);
            reportsRepository.save(reports);
            return ResponseEntity.created(new URI("/api/trainings/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
        }
        log.debug("You have not access to post training");
        return ResponseEntity.notFound().build();
    }

    private boolean isAdminOrCurrentUser(@RequestBody @Valid Training training) {
        return SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN) || (SecurityUtils.getCurrentUserLogin().isPresent() && SecurityUtils.getCurrentUserLogin().get().equals(training.getGroup().getCoach().getLogin()));
    }
    /**
     * PUT  /trainings : Updates an existing training.
     *
     * @param training the training to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated training,
     * or with status 400 (Bad Request) if the training is not valid,
     * or with status 500 (Internal Server Error) if the training couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trainings")
    @Timed
    public ResponseEntity<Training> updateTraining(@Valid @RequestBody Training training) throws URISyntaxException {
        log.debug("REST request to update Training : {}", training);
        if (training.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (isAdminOrCurrentUser(training)) {
            Training result = trainingRepository.save(training);
            trainingSearchRepository.save(result);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, training.getId().toString()))
                .body(result);
        }
        log.debug("You have not access to update training {}", training.getId());
        return ResponseEntity.notFound().build();
    }

    /**
     * GET  /trainings : get all the trainings.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of trainings in body
     */
    @GetMapping("/trainings")
    @Timed
    public List<Training> getAllTrainings() {
        log.debug("REST request to get all Trainings");
        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)){
            Optional<String> currentUserLogin = SecurityUtils.getCurrentUserLogin();
            if (currentUserLogin.isPresent()){
                List<Groups> groupsForCurrentUser = groupsRepository.findGroupsByCoach_Login(currentUserLogin.get());
                return trainingRepository.findByGroupIn(groupsForCurrentUser);
            }
        }
        return trainingRepository.findAll();
    }

    /**
     * GET  /trainings/:id : get the "id" training.
     *
     * @param id the id of the training to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the training, or with status 404 (Not Found)
     */
    @GetMapping("/trainings/{id}")
    @Timed
    public ResponseEntity<Training> getTraining(@PathVariable Long id) {
        log.debug("REST request to get Training : {}", id);
        Optional<Training> training = trainingRepository.findById(id);
        if (training.isPresent() && isAdminOrCurrentUser(training.get())) {
            return ResponseUtil.wrapOrNotFound(training);
        }
        log.debug("You have not access to get training {}", id);
        return ResponseEntity.notFound().build();
    }

    /**
     * DELETE  /trainings/:id : delete the "id" training.
     *
     * @param id the id of the training to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/trainings/{id}")
    @Timed
    public ResponseEntity<Void> deleteTraining(@PathVariable Long id) {
        log.debug("REST request to delete Training : {}", id);

        Optional<Training> training = trainingRepository.findById(id);
        if (training.isPresent() && isAdminOrCurrentUser(training.get())) {
            trainingRepository.deleteById(id);
            trainingSearchRepository.deleteById(id);
            return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
        }
        log.debug("You have not access to delete training {}", id);
        return ResponseEntity.notFound().build();
    }

    /**
     * SEARCH  /_search/trainings?query=:query : search for the training corresponding
     * to the query.
     *
     * @param query the query of the training search
     * @return the result of the search
     */
    @GetMapping("/_search/trainings")
    @Timed
    public List<Training> searchTrainings(@RequestParam String query) {
        log.debug("REST request to search Trainings for query {}", query);

        List<Training> trainingList = StreamSupport
            .stream(trainingSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            return trainingList;
        }
        Optional<String> currentUserLogin = SecurityUtils.getCurrentUserLogin();
        return trainingList.stream().filter(a -> !a.getGroup().getCoach().getLogin().equals(currentUserLogin.get())).collect(Collectors.toList());
    }

}
