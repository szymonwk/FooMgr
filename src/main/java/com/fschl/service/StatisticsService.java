package com.fschl.service;

import com.fschl.domain.Reports;
import com.fschl.repository.ReportsRepository;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional(readOnly = true)
public class StatisticsService {
    public enum statistics {
        AVG, MEDIAN, MEAN
    }

    @Autowired
    ReportsRepository reportsRepository;


    private Optional<List<Reports>> getReportsListForUser(String login, boolean isAdmin){
        if (isAdmin) {
            return Optional.ofNullable(reportsRepository.findAllByNumberOfParticipatsNotNull());
        }
        return Optional.ofNullable(reportsRepository.findAllByTrainingGroupCoachLoginAndNumberOfParticipatsNotNull(login));
    }

    private double getTrainingCounter(String login, boolean isAdmin){
        if (isAdmin){
            return (double) Optional.ofNullable(reportsRepository.findAll().size()).orElse(0);
        }
        return (double) Optional.ofNullable(reportsRepository.findAllByTrainingGroupCoachLogin(login).size()).orElse(0);
    }


    public Map<String, Long> getUserStatistics(String login, boolean isAdmin){
        SummaryStatistics stats = new SummaryStatistics();
        getReportsListForUser(login, isAdmin).ifPresent(a -> a.stream().filter(rap -> rap.getNumberOfParticipats() != null).forEach(report -> stats.addValue(report.getNumberOfParticipats().doubleValue())));
        Map<String, Long> statistics = new HashMap<>();
        statistics.put("mean", Double.valueOf(stats.getMean()).longValue());
        statistics.put("max", Double.valueOf(stats.getMax()).longValue());
        statistics.put("min", Double.valueOf(stats.getMin()).longValue());
        statistics.put("counter", Double.valueOf(getTrainingCounter(login, isAdmin)).longValue());
        return statistics;
    }

    public List<Long> getPredictions (String login, boolean isAdmin){
        SimpleRegression simpleRegression = new SimpleRegression(true);
        double counter = 1.0d;
        double size = 0;
        List<Double> predictionsList = new ArrayList<>();
        List<Long> displayPredictionList = new ArrayList<>();


        if (getReportsListForUser(login, isAdmin).isPresent()) {
            size = getReportsListForUser(login, isAdmin).get().size();
            for (Reports reports : getReportsListForUser(login, isAdmin).get()) {
                if (reports.getNumberOfParticipats()!=null) {
                    predictionsList.add((double) reports.getNumberOfParticipats());
                    simpleRegression.addData(counter, reports.getNumberOfParticipats());
                    counter++;
                }
            }
        }

        for (double i = size + 1; i < size + 5; i++) {
            displayPredictionList.add(Double.valueOf(simpleRegression.predict(i)).longValue());
            simpleRegression.addData(i, simpleRegression.predict(i));
        }

        return displayPredictionList;
    }

}
