package com.fschl.service.enums;

public enum ReportType {
    EXPENSE,
    PREDICTIONS
}
