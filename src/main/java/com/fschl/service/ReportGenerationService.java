package com.fschl.service;

import com.fschl.FooschoolApp;
import com.fschl.domain.Authority;
import com.fschl.domain.Reports;
import com.fschl.domain.User;
import com.fschl.repository.ReportsRepository;
import com.fschl.repository.UserRepository;
import com.fschl.service.enums.ReportType;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


@Service
public class ReportGenerationService {
    private static final Logger log = LoggerFactory.getLogger(ReportGenerationService.class);

    private static final int COACH_PERCENTAGE = 50;
    private static final int SINGLE_TRAINING_RATE = 20;

    @Autowired
    ReportsRepository reportsRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    public void generateReport(ReportType reportType, LocalDate date) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet(reportType.toString());
        Row header = sheet.createRow(0);
        CellStyle headerStyle = workbook.createCellStyle();
        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");
        font.setBold(true);
        headerStyle.setFont(font);

        switch (reportType) {
            case EXPENSE:
                generateExpense(sheet, header, headerStyle);
        }

        String fileName = date.minusMonths(1).getMonthValue() + "_" + date.minusMonths(1).getYear() + "_" + reportType;
        log.info("Report {} generated!", fileName);
        String fileLocation = fileName + ".xlsx";

        FileOutputStream outputStream = new FileOutputStream(fileLocation);
        workbook.write(outputStream);
        workbook.close();
    }

    @Transactional
    public void generateExpense(Sheet sheet, Row header, CellStyle headerStyle) {
        Cell headerCell = header.createCell(0);
        headerCell.setCellValue("Counter");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(1);
        headerCell.setCellValue("Name");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(2);
        headerCell.setCellValue("Surname");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(3);
        headerCell.setCellValue("Number of reports");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(4);
        headerCell.setCellValue("Number of participants");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(5);
        headerCell.setCellValue("Expense");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(6);
        headerCell.setCellValue("Min Presence");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(7);
        headerCell.setCellValue("Max Presence");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(8);
        headerCell.setCellValue("Mean Presence");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(9);
        headerCell.setCellValue("Total incomes");
        headerCell.setCellStyle(headerStyle);


        int currentRowSheet = 1;
        for (User currentUser : userRepository.findAll()) {
            Optional<User> userWithAuthorities = userService.getUserWithAuthorities(currentUser.getId());
            Authority authority = new Authority();
            authority.setName("ROLE_ADMIN");
            if (!userWithAuthorities.get().getAuthorities().contains(authority) && !userWithAuthorities.get().getLogin().equals("anonymoususer")) {
                SummaryStatistics stats = new SummaryStatistics();
                List<Reports> reportsInLastMonth =
                    reportsRepository.findAllByTrainingNextTrainingDateAfterAndTrainingGroupCoachLoginOrderByTrainingNextTrainingDateAsc(LocalDate.now().minusMonths(1), currentUser.getLogin());

                long sumNumberOfParticipants = 0;
                for (Reports singleReport : reportsInLastMonth) {
                    if (singleReport.getNumberOfParticipats()!=null) {
                        stats.addValue(singleReport.getNumberOfParticipats());
                        sumNumberOfParticipants = singleReport.getNumberOfParticipats() + sumNumberOfParticipants;
                    }
                }

                long expense = 0;
                long incomes = 0;

                if (sumNumberOfParticipants > 0) {
                    incomes = sumNumberOfParticipants * SINGLE_TRAINING_RATE;
                    expense = incomes * COACH_PERCENTAGE / 100;
                }

                Row currentRow = sheet.createRow(currentRowSheet);

                Cell currentUserCell = currentRow.createCell(0);
                currentUserCell.setCellValue(currentRowSheet);

                Cell currentUserNameCell = currentRow.createCell(1);
                currentUserNameCell.setCellValue(currentUser.getFirstName());

                Cell currentUserSurnameCell = currentRow.createCell(2);
                currentUserSurnameCell.setCellValue(currentUser.getLastName());

                Cell numberOfReports = currentRow.createCell(3);
                numberOfReports.setCellValue(reportsInLastMonth.size());

                Cell numberOfParticipants = currentRow.createCell(4);
                numberOfParticipants.setCellValue(sumNumberOfParticipants);

                Cell expenseForCoach = currentRow.createCell(5);
                expenseForCoach.setCellValue(expense);

                Cell minPresence = currentRow.createCell(6);
                minPresence.setCellValue(stats.getMin());

                Cell maxPresence = currentRow.createCell(7);
                maxPresence.setCellValue(stats.getMax());

                Cell meanPresence = currentRow.createCell(8);
                meanPresence.setCellValue(stats.getMean());

                Cell totalIncomes = currentRow.createCell(9);
                totalIncomes.setCellValue(incomes);

                currentRowSheet++;
            }
        }
    }
}
