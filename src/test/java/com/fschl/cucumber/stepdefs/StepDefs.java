package com.fschl.cucumber.stepdefs;

import com.fschl.FooschoolApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = FooschoolApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
